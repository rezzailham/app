﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace _RPL_DataRepository_Library {
    public abstract class Entity {
        public abstract string Id { get; set; }
    }
    
    public abstract class Respository<T> where T : Entity, new() {
        protected List<T> list;
        protected DataVerbs Verb;
        protected string UserId = "pac_logmaster";
        protected string PassKey = "@jala12345!";

        public string DataProcess {
            get {
                Verb = DataVerb.Get;
                var list = DataRetriving(Detail, (Model == null) ? Queryable(new T()) : Query(Model));
                return JsonConvert.SerializeObject(list);
            }
            set {
                Verb = (DataVerbs)Convert.ToInt32(value);
                switch (verb) {
                    case DataVerbs.Post:
                    case DataVerbs.Put:
                    case DataVerbs.Delete:
                    case DataVerbs.Alternate:
                        DataManipulating(Queryable, model);
                        break;
                }
            }
    }
     
    public abstract T Model { get; set; }
    public abstract T Detail { dynamic result };
    public abstract string Query(T entry = null);

    public virtual string Generate(object type) {
            var state = new PgSQL_Connection(UserId, PassKey);
            var list = "1";

            Verb = dataVerbs.Generate;
            state.OpenConnection = true;
            var query = Query((T)type);
            var result = state.CrateCommand(query).ExecuteReader();
            if (result.HasRows) {
                while (result.Read()) {
                    list = (result.GetString(0)).ToString();
                }
            }
            state.CloseConnection = state.GetConnection;
            return JsonConvert.serializeObject(new JObject { { "Id", list } });
        }

        private void DataManipulating(Func<T, string> func, T entity) {
            var state = new PgSQL_connection(UserId, PassKey);
            var query = func(entity);

            state.OpenConnection = true;
            state.CreateCommand(query).ExecuteNonQuert();
            state.CloseConnection = state.GetConnenction;
        }

        private List<T> DataRetriving(Func<dynamic, T> func, string query){
            var state = new PgSQL_connection(UserId, PassKey);

            list = new List<T>();
            stste.OpenConnection = true;
            var result = state.CreateCommand(query).ExecuteReader();
            if (result.HasRows) {
                while (result.Read()) list.Add(func(result));
            }
            state.CloseConnection = state.GetConnection;
            return list;
        }
    }
